using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using _08CodeFirst.Entities;

namespace _08CodeFirst.DbsContext
{
    public class CarContext : DbContext
    {
        public CarContext(DbContextOptions<CarContext> options) : base(options) {}

        public DbSet<CarsModel> Cars{get;set;}
        public DbSet<OwnnerModel> Ownners{get;set;}


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //给OwnnerModel添加主键
            modelBuilder.Entity<OwnnerModel>(entity=>{
                entity.HasKey(o=>o.OwnnerId);  //主键
            });

            //外键 c->o (1)  o->c (n)
            modelBuilder.Entity<CarsModel>(entity=>{
                entity.HasOne(c=> c.OwnnerModel).WithMany(o=>o.carsModels)
                .HasForeignKey(c=>c.OwnnerId);
            });
            //设置CarType为非空，最大长度为20
            modelBuilder.Entity<CarsModel>(entity=>{
                entity.Property(c=>c.CarType).IsRequired()
                .HasColumnType("varchar(20)");

                entity.Property(c=>c.IsDeleted).HasDefaultValue(false);
            });
            //false：可以显示，true：表示删除
            //HasQueryFilter(IsDeleted):如果返回的是true，那么显示该条数据
            modelBuilder.Entity<CarsModel>().HasQueryFilter(c=> !c.IsDeleted);

      

            //设置数据种子(初始数据)
            modelBuilder.Entity<OwnnerModel>().HasData(
                new OwnnerModel(){OwnnerId=1,OwnnerName="关羽"},
                new OwnnerModel(){OwnnerId=2,OwnnerName="小橘子"}
            );

            modelBuilder.Entity<CarsModel>().HasData(
                new CarsModel(){CarId=1,OwnnerId=1,CarType="马车",CarName="赤兔"},
                new CarsModel(){CarId=2,OwnnerId=2,CarType="跑车",CarName="黄金剃刀"},
                new CarsModel(){CarId=3,OwnnerId=2,CarType="公交",CarName="双层巴士"}
            );
        }
    }
}