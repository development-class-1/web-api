﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace _08CodeFirst.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ownner",
                columns: table => new
                {
                    OwnnerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OwnnerName = table.Column<string>(type: "varchar(200)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ownner", x => x.OwnnerId);
                });

            migrationBuilder.CreateTable(
                name: "Cars",
                columns: table => new
                {
                    CarId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OwnnerId = table.Column<int>(type: "int", nullable: false),
                    CarType = table.Column<string>(type: "varchar(20)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    CarName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cars", x => x.CarId);
                    table.ForeignKey(
                        name: "FK_Cars_Ownner_OwnnerId",
                        column: x => x.OwnnerId,
                        principalTable: "Ownner",
                        principalColumn: "OwnnerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Ownner",
                columns: new[] { "OwnnerId", "OwnnerName" },
                values: new object[,]
                {
                    { 1, "关羽" },
                    { 2, "小橘子" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "CarId", "CarName", "CarType", "OwnnerId" },
                values: new object[,]
                {
                    { 1, "赤兔", "马车", 1 },
                    { 2, "黄金剃刀", "跑车", 2 },
                    { 3, "双层巴士", "公交", 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cars_OwnnerId",
                table: "Cars",
                column: "OwnnerId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cars");

            migrationBuilder.DropTable(
                name: "Ownner");
        }
    }
}
