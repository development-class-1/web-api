using Microsoft.EntityFrameworkCore;
using _08CodeFirst.DbsContext;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddCors(c=>c.AddPolicy("any",p=>p.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod()));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var configuration = builder.Configuration;

builder.Services.AddDbContext<CarContext>(option=>{
    var res = configuration.GetConnectionString("Default");
    // Console.WriteLine(res);
    option.UseSqlServer(configuration.GetConnectionString("Default"));
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();
app.UseCors("any");
app.MapControllers();

app.Run();
