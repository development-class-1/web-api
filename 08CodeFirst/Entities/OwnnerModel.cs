using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using _08CodeFirst.Entities;

namespace _08CodeFirst.Entities
{
    [Table("Ownner")]
    public class OwnnerModel
    {
        [Key]
        public int OwnnerId { get; set; } //默认约定:Id  entity+Id OwnnerModelId

        [Required]
        [Column(TypeName ="varchar(200)")]
        public string? OwnnerName { get; set; }
        //
        // [JsonIgnore]
        public List<CarsModel>? carsModels{get;set;}

    }
}