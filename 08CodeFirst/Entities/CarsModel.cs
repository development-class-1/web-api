using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using _08CodeFirst.Entities;
//默认约定 数据注释，fluentAPI
namespace _08CodeFirst.Entities
{
    public class CarsModel
    {
        //识别Id就是主键

        //数据注释  fluentAPI
        //默认自增
        [Key]
        public int CarId { get; set; }  //主键
        [ForeignKey("OnnerId")]
        public int OwnnerId{get;set;}  //外键
        public string? CarType { get; set; } //不可以为空


        public Boolean IsDeleted{get;set;} //是否删除的标志

        public string CarName {get;set;}=null!;

        // public string? CarOwnner{get;set;} // 可以为空

        //导航属性
        public OwnnerModel? OwnnerModel{get;set;}
    }
}