using Microsoft.AspNetCore.Mvc;
using _08CodeFirst.DbsContext;
using Microsoft.EntityFrameworkCore;

namespace _08CodeFirst.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class WeatherForecastController : ControllerBase
{

    private readonly CarContext _context;
    public WeatherForecastController(CarContext context)
    {
        this._context = context;
    }

    [HttpGet]
    public IActionResult Cars()
    {
        var res = _context.Cars.Include(c => c.OwnnerModel).Select(c => new
        {
            CardId = c.CarId,
            CarOwnner = c.OwnnerModel.OwnnerName,
            CarName = c.CarName,
            CarType = c.CarType,
        }).ToList();

        if (res != null)
        {
            return Ok(res);
        };
        return NotFound();
    }


    [HttpGet]
    public IActionResult Get(string ownnerName)
    {
        //查询关羽名下有什么车  res.OwnnerId
        // var res = _context.Ownners.FirstOrDefault(o=>o.OwnnerName==ownnerName);
        // var carsResult = _context.Cars.Where(c=>c.OwnnerId==res.OwnnerId).ToList();

        //查询了几次数据库?   延迟
        var carsResult1 = _context.Ownners.Where(o => o.OwnnerName == ownnerName).Select(o => o.carsModels).FirstOrDefault();

        //预加载:Include()  预加载
        var carsResult = _context.Ownners.Include(o => o.carsModels).Where(o => o.OwnnerName == ownnerName).Select(o => o.carsModels).FirstOrDefault();

        //多表预加载
        // var carsResult = _context.Ownners.Include(o=>o.carsModels).ThenInclude().Where(o=>o.OwnnerName==ownnerName);




        if (carsResult != null && carsResult.Count > 0)
        {
            return Ok(carsResult);
        }
        return NotFound();
    }


    [HttpDelete("{id}")]
    public void Cars(int id){
        if(id!=null){
            var res = _context.Cars.Find(id);
            //硬删除
           /*  _context.Cars.Remove(res); */
           res.IsDeleted = true;
           _context.Update(res);
            _context.SaveChanges();
        }
    }
}
