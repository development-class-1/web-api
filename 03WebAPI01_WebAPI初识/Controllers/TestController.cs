using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI01.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]   //通配路由规则
    public class NextController : ControllerBase
    {
        
        [HttpGet] //get请求

        //控制器动作，行为 ： action
        //MVC:
        public string Get(string name,string age)
        {
            return $"{name},你好";
        }

        [HttpGet]
        public string Get1(){
            return "很好";
        }

    }
}