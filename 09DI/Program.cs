using _09DI.Services;
using _09DI.Services.IServices;

var builder = WebApplication.CreateBuilder(args);

/* 
Milk milk = new Milk();
Tea tea = new Tea(milk);
MilkTea milkTea = new MilkTea(tea);

 */


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//注册:瞬态 AddSingleton,  AddScoped
// builder.Services.AddTransient<MilkTea>();
// builder.Services.AddTransient<Tea>();
// builder.Services.AddTransient<Milk>();

//<TService,IImplementation >:<服务类型(一般是一个接口)，，实现该接口的类>
builder.Services.AddTransient<IMilkTea,MilkTea>();
builder.Services.AddTransient<IMilk,Milk>();
builder.Services.AddTransient<ITea,Tea>();





var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
