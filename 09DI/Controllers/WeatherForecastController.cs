using Microsoft.AspNetCore.Mvc;
using _09DI.Services;
namespace _09DI.Controllers;
using _09DI.Services.IServices;

[ApiController]
[Route("[controller]/[action]")]
public class WeatherForecastController : ControllerBase
{
    private readonly IMilkTea mt;

    //注入到构造函数中
    //创建了一个实例
    public WeatherForecastController(IMilkTea milkTea){
        this.mt = milkTea;
    }

    [HttpGet]
    public string get(){
        // var tea = new MilkTea();
        return mt.getTea();
    }
}
