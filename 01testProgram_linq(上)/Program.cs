﻿/* 练习1 */

/*
var lst = new List<int>{1,2,3,40,5,6,7,8,9};
 List<int> result = new List<int>();  //lst.Count
for (int i = 0; i < lst.Count; i++)
{
    if(lst[i] > 3 && lst[i] % 2 == 0){
        result.Add(lst[i]);
    }
}
result.Sort();
for (int i = 0; i < result.Count; i++)
{
    Console.Write($"{result[i]} > ");
} */



//linq(语言集成查询);
//linq：查询表达式(伪SQL语句), 链式(扩展方法);



//T-SQL: select * from lst where [lst[i] > 3 && lst[i] % 2 == 0] orderby (条件);
//1.查询表达式 ，特点：以select，groupby结尾，
/* var res =   from n in lst
            where n>3 && n%2==0
            orderby n
            select n; */

//2.链式表达式  MVC
//var res1 = lst.Where(n=> n>3 && n%2==0).OrderBy(n => n).Select(n=>n);


//linq特性：延迟性,惰性（defer）， 消耗性(exhaust) 
//常用的方法:Where()/Select()/OrderBy()/GroupBy()/Single()/First();
