
//c#: 隐式声明变量var， lambda:表达式(箭头函数) ， linq（）   
//集合 增删改查(linq:语言集成查询)
//创建一个list集合
/* List<int> lst = new List<int>();
List<int> lst1 = new List<int>(100);  */
var lst = new List<int>{1,2,3,4,5};

var arr = new int[5]{6,7,8,9,0};
//添加:末尾添加
//lst.Add(6);
lst.AddRange(arr);

//插入insert(),insertRange()

//删除remove      清空：clear


//改 lst[index] = target
// Console.WriteLine(lst[0]);



//遍历集合
foreach (var item in lst)
{
    Console.Write($"{item}  ");
}
