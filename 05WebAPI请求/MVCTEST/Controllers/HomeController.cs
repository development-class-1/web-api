﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MVCTEST.Models;

namespace MVCTEST.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        var students =new List<StudentModel>(){
            new StudentModel(){StuId=1,Name="张三"},
            new StudentModel(){StuId=2,Name="李四"}
        };
        //ViewData  ViewBag
        //强类型视图
        return View(students);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
