using System.Collections.ObjectModel;
using System.Data;
using Serilog;
using Serilog.Sinks.MSSqlServer;
using TestLog.Controllers;
using TestLog.Entities;
using TestLog.Filters;
// using Serilog.Settings;

// Log.Logger = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger();






/* Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Information()
    .WriteTo.Console()
    .WriteTo.File(
        "Logs/log-.log",
    rollingInterval: RollingInterval.Day)  //文件名已有:追加日志记录  没有文件名，重新创建
    .CreateLogger(); */

var builder = WebApplication.CreateBuilder(args);


//从appsettings中获取相关配置
/* var config = builder.Configuration;
Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(config)
    .CreateLogger(); */


/* var sinksOpt = new MSSqlServerSinkOptions();
sinksOpt.TableName = "MyLog";
sinksOpt.AutoCreateSqlDatabase = true;
sinksOpt.AutoCreateSqlTable = true; */
//表的设计
/* var columnOpt = new ColumnOptions{
    AdditionalColumns = new Collection<SqlColumn>{
        new SqlColumn{ColumnName = "EnvironmentUserName", PropertyName = "UserName", DataType = SqlDbType.NVarChar, DataLength = 64,AllowNull=true}
    }
    
}; */
/* columnOpt.Store.Remove(StandardColumn.Properties);
columnOpt.Store.Remove(StandardColumn.MessageTemplate);
columnOpt.Store.Add(StandardColumn.LogEvent); */


//将日志记录存储在DB中
/* Log.Logger = new LoggerConfiguration()
.WriteTo.Console()
.WriteTo.MSSqlServer(
    // /Integrated Security=SSPI:Windows身份验证  sqlserver身份验证: efcore
    connectionString:builder.Configuration.GetConnectionString("SerilogConnection"),
    sinkOptions:sinksOpt,
    columnOptions:columnOpt

)
.CreateLogger();
 */

/* var configuration =new  ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json")
    .Build(); */


Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .ReadFrom.Configuration(builder.Configuration) //读取appsettings.jsonzhong中的配置信息
    .CreateLogger();



var test = new {Class = 3, Name="刘备"};
var arr = new string[2]{"apple","banana"};
Log.Information("{@text},{$arr}",test,arr);

Log.Warning("test");

//清除内置的日志记录  
builder.Logging.ClearProviders();

builder.Host.UseSerilog();



// Add services to the container.
builder.Services.AddControllers();
/* builder.Services.AddControllers(option=>{
    option.Filters.Add(typeof(TestActionFilterAttribute));
}); */
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddTransient<UserEntity>();
builder.Services.AddTransient<TestActionFilterAttribute>();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseSerilogRequestLogging();

app.UseAuthorization();

app.MapControllers();


app.Run();



