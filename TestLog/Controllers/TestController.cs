using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestLog.Entities;
using TestLog.Filters;

namespace TestLog.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private readonly ILogger<TestController> _logger;

        public TestController(ILogger<TestController> logger)
        {
            _logger = logger;
        }


        [HttpPost]
        [ServiceFilter(typeof(TestActionFilterAttribute))]  //有没有生效?
        public UserEntity post(UserEntity user)
        {
            
            return user;
        }
        [HttpPost]
       
        public UserEntity post1([FromBody]UserEntity user)
        {
            
            return user;

        }
    }
}