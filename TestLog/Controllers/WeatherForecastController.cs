using Microsoft.AspNetCore.Mvc;
using TestLog.Entities;

namespace TestLog.Controllers;

[ApiController]  //Atti
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    public IEnumerable<WeatherForecast> Get(int id)
    {
        _logger.LogInformation("开始天气预报");
        try{
            for (int i = 0; i < 50; i++)
            {
                _logger.LogInformation("{i}",i);
                if(i==25){
                    throw new Exception();
                }
            }
        }
        catch(Exception ex){
            _logger.LogError(ex,"出现错误");
        }


        return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        {
            Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            TemperatureC = Random.Shared.Next(-20, 55),
            Summary = Summaries[Random.Shared.Next(Summaries.Length)]
        })
        .ToArray();
    }

    [HttpPost]
    public IActionResult post(UserEntity user){
        return Ok(user);
    }


}
