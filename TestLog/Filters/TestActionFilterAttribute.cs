using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using TestLog.Controllers;
using TestLog.Entities;

namespace TestLog.Filters
{
    //特性
    public class TestActionFilterAttribute : Attribute,IActionFilter
    {
        private UserEntity _user;
        private ILogger<TestActionFilterAttribute> _logger;

        //构造函数只进行初始化
        //生命周期：创建阶段:2个钩子函数:创建前，创建后

        public TestActionFilterAttribute(ILogger<TestActionFilterAttribute> logger,UserEntity user)
        {
            _user = user;
            _logger = logger;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            //throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var path = context.HttpContext.Request.Path;

            //获取User的第一种方式
            // _logger.LogInformation("111{user}",_user.UserId);
            
            var args = context.ActionArguments;

            //throw new NotImplementedException();
            // _logger.Infromation("{u}正在访问。。。。",UserEntity.UserName);
        }
    }
}