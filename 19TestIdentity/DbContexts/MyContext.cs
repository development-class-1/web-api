using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _19TestIdentity.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace _19TestIdentity.DbContexts
{
    public class MyContext : IdentityDbContext<User,Role,int>
    {
        public MyContext(DbContextOptions<MyContext> options):base(options)
        {
            
        }
    }
}