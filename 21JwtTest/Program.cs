using System.Text;
using _21JwtTest;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    var schemeName = "Bearer";
    options.AddSecurityDefinition(schemeName, new OpenApiSecurityScheme
    {
        Description = "格式 Bearer [token]",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey
    });

    options.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = schemeName
                            }
                        },
                        new string[0]
                    }
                });
});








// var section = builder.Services.Configure<JwtOption>(builder.Configuration.GetSection("JwtOption"));
var config = builder.Services.BuildServiceProvider().GetRequiredService<IConfiguration>();

var tokenSection = config.GetSection(nameof(JwtOption));
//Jwt服务注册
builder.Services.Configure<JwtOption>(tokenSection);
//获取实例
var tokenOptions = tokenSection.Get<JwtOption>();

//注册鉴权  
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(opt =>
    {
        opt.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true, //是否在令牌期间验证签发者
            ValidateAudience = true, //是否验证接收者
            ValidateLifetime = true, //是否验证失效时间
            ValidateIssuerSigningKey = true, // 是否验证签名
            ValidAudience = tokenOptions.Audience, //接收者
            ValidIssuer = tokenOptions.Issuer, //签发者
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenOptions.SecKey!)) // 秘钥
        };
    });














var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseAuthentication();  //授权
app.UseAuthorization();  //鉴权

app.MapControllers();

app.Run();
