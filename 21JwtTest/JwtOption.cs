

namespace _21JwtTest
{
    public class JwtOption
    {
        public string? SecKey { get; set; }
        public int ExpireTime { get; set; }
        public string? Audience { get; set; }
        public string? Issuer { get; set; }
    }
}