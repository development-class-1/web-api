using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace _21JwtTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IOptions<JwtOption> _jwtOption;

        public AuthController(IOptions<JwtOption> jwtOption)
        {
            _jwtOption = jwtOption;
        }

        [HttpGet("login")]
        public IActionResult Get(string role)
        {
            //生成Jwt令牌流程
            //1.声明payload
            var claims = new[]{
                new Claim(ClaimTypes.Role, role)
        };
            //2.生成秘钥
            var cred = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOption.Value.SecKey!)),
        SecurityAlgorithms.HmacSha256
            );

            //3.token
            var token = new JwtSecurityToken(
                issuer: _jwtOption.Value.Issuer,
                audience: _jwtOption.Value.Audience,
                claims: claims,  //payload
                expires: DateTime.Now.AddSeconds(_jwtOption.Value.ExpireTime),
                signingCredentials: cred
            );

            //4.生成jwt令牌
            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return Ok(jwt);
        }

        //admin才能进
        [HttpGet("admin")]
        [Authorize(Roles = "admin")]
        public IActionResult GetAdmin()
        {
            return Ok("欢迎,管理员");
        }
        //只有normal能进
        [HttpGet("user")]
        [Authorize(Roles = "normal")]
        public IActionResult GetUser()
        {
            return Ok("欢迎,用户");
        }


    }
}