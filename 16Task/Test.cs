using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _16Task
{
    public class Test
    {
        //模拟下载三张图片 1 2 3 => 
        public static async Task GetFiles(){
            var imgList = new List<string>{
                "http://text.example/img1.png",  //1
                "http://text.example/img2.png",  //2
                "http://text.example/img3.png"   //3
            };

            //任务列表
            var taskList = new List<Task>();

            foreach (var img in imgList)
            {
                var task = DownLoadAsync(img); //当前下载任务
                taskList.Add(task);
            }

            await Task.WhenAll(taskList); //集合中的所有线程执行完之后，程序才往下走

        }

        //模拟下载returns : Class Task
        public static async Task DownLoadAsync(string img){
            System.Console.WriteLine("开始下载"+img.Split('/').Last());
            await Task.Delay(2000);  //下载过程
            System.Console.WriteLine(img.Split('/').Last()+"下载完成");
        }

    }
}