using System.Text.Json;
using System.Text.Json.Serialization;
using _07EFCore.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace _07EFCore.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class WeatherForecastController : ControllerBase
{
    private OrderInfoContext _context { get; set; }
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    public WeatherForecastController(OrderInfoContext context)
    {

        this._context = context;
        //为当前控制器的所有动作设置取消实体追踪
        // _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
    }



    //根据id查药品
    /*     [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            //打开了数据库连接池
            using var context = new OrderInfoContext();   //用完即释放
            //Linq:扩展方法
            var res = context.OrderInfos.Find(id); //Find(id):根据主键查询
            // context.Dispose(); //关闭数据库连接池
            return Ok(res);
        } */

    [HttpPut("{id}")]
    public IActionResult Orders(int id)
    {
        //打开了数据库连接池
        using var context = new OrderInfoContext();   //用完即释放

        //一整条数据
        var res = context.OrderInfos.AsNoTracking().FirstOrDefault(o => o.Id == id); //Find(id):根据主键查询

        if (res is null)
        {
            return NotFound();
        }
        //九味地黄丸->圣宝片
        res.MedicineName = "乌鸡白凤丸";  //默认开启实体追踪,但是内存开销比较大 查询(70%)
        context.Update(res);
        //保存更新到数据库中
        context.SaveChanges();
        return Ok(res);
    }

    //找到admin下单列表
    [HttpGet]
    public IActionResult Orders(string name)
    {
        // using var context = new OrderInfoContext();
        //为当前的上下文实例取消实体追踪功能

        //1.根据名字查UserId
        // var user = _context.UserInfos.FirstOrDefault(u => u.UserName == name);
        //2.根据UserId查对应订单
        // var orders = _context.OrderInfos.Where(o => o.UserId == 1).ToList();

        _context.OrderInfos.Include(o=> o.User).Where(o=>o.User.UserName==name);

        // if (orders != null) return Ok(orders);
        
        return NotFound();
    }

    //查询所有药品
    [HttpGet]
    public async Task<IActionResult> GetON()
    {
        // using var context=new OrderInfoContext();
        var res = await _context.OrderInfos.ToListAsync();
        return Ok(res);
    }
}
