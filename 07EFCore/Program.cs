using Microsoft.EntityFrameworkCore;
using _07EFCore.Entities;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
//全局注册上下文
// builder.Services.AddScoped<OrderInfoContext>();

var configuration = builder.Configuration;
builder.Services.AddDbContext<OrderInfoContext>(option=>{
    //设置全局不追踪
    option.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
    //设置连接配置
    option.UseSqlServer(configuration.GetConnectionString("Default"));
});



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
