﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace _07EFCore.Entities;

public partial class OrderInfoContext : DbContext
{
    public OrderInfoContext()
    {
    }

    public OrderInfoContext(DbContextOptions<OrderInfoContext> options)
        : base(options)
    {
    }

    public virtual DbSet<OrderInfo> OrderInfos { get; set; }  //一个Dbset一般对应到数据库中就是一张表

    public virtual DbSet<UserInfo> UserInfos { get; set; }

    // private readonly string connectConfig = "Server=127.0.0.1;Database=OrderInfo;uid=sa;pwd=123456;TrustServerCertificate=True";
    // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    // {
    //     optionsBuilder.UseSqlServer(connectConfig);
    // }
    //默认约束<<数据注释<<fluentAPI
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<OrderInfo>(entity =>
        {
            entity.HasIndex(e => e.UserId, "IX_OrderInfos_UserId");

            entity.Property(e => e.AddTime).HasColumnType("datetime");
            entity.Property(e => e.SendTime).HasColumnType("datetime");

            entity.HasOne(d => d.User).WithMany(p => p.OrderInfos).HasForeignKey(d => d.UserId);
        });

        modelBuilder.Entity<UserInfo>(entity =>
        {
            entity.HasKey(e => e.UserId);

            entity.Property(e => e.PassWord)
                .HasMaxLength(20)
                .IsUnicode(false);
            entity.Property(e => e.UserName)
                .HasMaxLength(300)
                .IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
