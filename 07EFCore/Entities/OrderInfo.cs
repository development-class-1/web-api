﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace _07EFCore.Entities;

public partial class OrderInfo
{
    public int Id { get; set; }

    public string MedicineName { get; set; } = null!;

    public int UserId { get; set; }

    public DateTime AddTime { get; set; }

    public bool SendType { get; set; }

    public int State { get; set; }

    public DateTime? SendTime { get; set; }

    [JsonIgnore]
    public virtual UserInfo User { get; set; } = null!; //表名不可以为空
}
