using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _12DI详解.Services.IServices;
using Microsoft.AspNetCore.Mvc;

namespace _12DI详解.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {


        public TestController(IServiceTest1 test1, IServiceTest2 test2)
        {
            this.SrvTest1 = test1;
            this.SrvTest2 = test2;
            // SrvTest1.ScopeService.Count(); //1
            // SrvTest2.ScopeService.Count(); //2
             SrvTest2.SingletonService.Count();  //1      1
             SrvTest2.SingletonService.Count();  //2      1
        }

        public IServiceTest1 SrvTest1 { get; }
        public IServiceTest2 SrvTest2 { get; }

        [HttpGet("{id}")]
        public int get(int id)
        {
            return id;
        }
    }
}