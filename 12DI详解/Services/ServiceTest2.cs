using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _12DI详解.Services.IServices;

namespace _12DI详解.Services
{
    public class ServiceTest2 : IServiceTest2
    {
        public ISingletonService SingletonService { get; }
        public IScopeService ScopeService { get; }
        public ITransientService TransientService { get; }

        public ServiceTest2(ISingletonService singletonService,
            IScopeService scopeService, ITransientService transientService)
        {
            SingletonService = singletonService;
            ScopeService = scopeService;
            TransientService = transientService;
        }


    }
}