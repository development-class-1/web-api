using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _12DI详解.Services.IServices;

namespace _12DI详解.Services
{
    public class ScopeService : IScopeService
    {

        public int Num { get; set; } = 0;

        public ScopeService()
        {

        }

        public void Count()
        {
            Num++;
        }


    }
}