using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _12DI详解.Services.IServices;
//using dmeo.api.Services;

namespace _12DI详解.Services
{
    public class ServiceTest1 : IServiceTest1
    {

        public ISingletonService SingletonService { get; }
        public IScopeService ScopeService { get; }
        public ITransientService TransientService { get; }

        public ServiceTest1(ISingletonService singletonService,
                            IScopeService scopeService,
                            ITransientService transientService)
        {
            SingletonService = singletonService;
            ScopeService = scopeService;
            TransientService = transientService;
        }


    }
}