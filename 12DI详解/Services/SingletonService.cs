using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _12DI详解.Services.IServices;

namespace _12DI详解.Services
{
    public class SingletonService :ISingletonService
    {

        public int Num { get; set; }
        public SingletonService(string name)
        {
            
        }

        public void Count()
        {
            Num++;
        }
    }
}