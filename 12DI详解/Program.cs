using _12DI详解.Services;
using _12DI详解.Services.IServices;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddTransient<IServiceTest1, ServiceTest1>();
builder.Services.AddTransient(typeof(IServiceTest1), typeof(ServiceTest1));
// builder.Services.AddTransient<IServiceTest2, ServiceTest2>();
// builder.Services.AddScoped<IScopeService, ScopeService>();
builder.Services.AddScoped(typeof(IScopeService), typeof(ScopeService));
builder.Services.AddTransient<ITransientService, TransientService>();
builder.Services.AddSingleton<ISingletonService, SingletonService>();
builder.Services.AddSingleton(typeof(ISingletonService), typeof(SingletonService));
//Singleton
builder.Services.AddSingleton<ISingletonService>(new SingletonService("张三"));


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
