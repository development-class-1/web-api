using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _17Test
{
/*     声明一个泛型接口 IMoveable<T>，其中包括 MoveForward() 和 TurnLeft() 方法，用于表示各种类型物品在平面环境中移动的行为。
示例：IMoveable<Robot> 接口可以表示机器人的移动方式，MoveForward() 方法可让机器人向前移动，并支持连续调用以实现连续运动效果。TurnLeft() 可以使它向左转弯。
使用该接口实现机器人和汽车的移动 */
    public interface IMove<T> where T : class,new() //约束：表明T是一个类，需要带无参构造
    {
        public void MoveForward();
    }
}