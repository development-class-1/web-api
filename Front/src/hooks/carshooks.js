import { ref, watch } from 'vue'
import { getCarsInfo, deleteCar } from '@/api/cars'

const isFecthing = ref(false);

//hooks:钩子函数(自定义)  use+...
export const useCars = () => {
    const carList = ref(null);

    const _getCarsInfo = () => {
        if (isFecthing.value) {
            return;
        }
        isFecthing.value = true;
        getCarsInfo().then(res => {
            carList.value = res;
            console.log(res);
            isFecthing.value = false;
        })
    }
    _getCarsInfo();



    const DeleteCarById = async (id) => {
       if(confirm("确认删除吗")){
        var res = await deleteCar(id);
        console.log(res);
        if(res.status == 200 && res.statusText == "OK"){
            alert("删除成功");
            //刷新
            //window.location.reload() // BOM: 刷新时候，回到顶部
            _getCarsInfo(); //性能较好
        }
       }
        
    }



    return { carList, DeleteCarById }
}
