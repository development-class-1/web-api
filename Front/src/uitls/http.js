import axios from 'axios';


//创建axios实例
const http = axios.create({
    baseURL:"http://localhost:5064/WeatherForecast",
    timeout:3000
});

export default http;