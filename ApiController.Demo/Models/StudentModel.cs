using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiController.Demo.Models
{
    public class StudentModel
    {
        public int Id{get;set;}

        [MaxLength(6)] //限制Name的长度最长是6字节
        public string? Name{get;set;}

        public string? ClassName{get;set;}
    }
}