using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiController.Demo.Models;

namespace ApiController.Demo.Controllers
{
    [ApiController] 
    [Route("api/[controller]/[action]")]
    public class TestController : ControllerBase
    {
        [HttpPost]
        public string PostTest(StudentModel model){
            return $"{model.ClassName}";
        }

       /*  [HttpPut]
        public void PutTest(int id, EditClassModel model){
            
        } */

        //string int IEnumerable<StudentModel>

        /* [HttpGet]
        public IEnumerable<StudentModel> Students(){
            return Enumerable.Range(0,5).Select(_ =>
                new StudentModel(){
                    Id=1,
                    Name="张三",
                    ClassName="1班"
                }
            );
        } */

        //既需要返回状态码，又需要数据

        [HttpGet("{id}")]
        public IActionResult Students(int id){
            
            var students =new List<StudentModel>(){
            new StudentModel(){Id=1,Name="张三",ClassName="1班"},
            new StudentModel(){Id=2,Name="李四",ClassName="2班"}
        };

        //linq
        var stu = students.FirstOrDefault(c=> c.Id == id);

        if(stu is null){
            return  NotFound();
        }else{
            return Ok(stu);
        }


            
        }

    }
}