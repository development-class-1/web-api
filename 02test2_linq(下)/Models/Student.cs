using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test2.Models
{
    public class Student
    {
        public int StuId{get;set;}
        public string? StuName{get;set;}

        public Boolean Gender{get;set;}

        public int ClassId{get;set;}
    }
}