using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test2.Models
{
    public class StuClass
    {
        public string? StuName{get;set;}

        public string? Gender{get;set;}

        public string? ClassName{get;set;}
    }
}