﻿using test2.Models;

var rnd = new Random(1000);
var arr = Enumerable.Range(0,200).Select(_ => rnd.Next(0,10));

//扩展方法-> linq   
var numD = arr.GroupBy(n =>n).ToDictionary(group=> group.Key, value=> value.Count());
//查询表达式 -> linq
var res =   from n in arr
            group n by n into g
            //既要键，又要次数  类:Student学生类（gender）（嘉远）  new Student(){gender=..}
            select new {g.Key,Count=g.Count()};

var stuList = new List<Student>(){
    new Student(){StuId=1,StuName="张三",Gender=false},
    new Student(){StuId=2,StuName="李四",Gender=true},
    new Student(){StuId=3,StuName="王五",Gender=false}
};

//姓名+性别
var res1 = from stu in stuList
            select new Student(){
                StuName= stu.StuName,
                Gender = stu.Gender
                };

/* foreach (var item in res1)
{
    Console.WriteLine($"{item.StuName},{item.Gender}");
} */


//
var mulArr =new int [][]{
    new int[]{1,2,3},
    new int[]{4,5,6},
    new int[]{7,8,9}
};
//2维->转成1维
var res2 =  from arr1 in mulArr
            from num in arr1
            select num;


var langArr = new string[]{"rust","python","java","csharp","c","golang"};
//统计字符出现的次数
/* var res3 =  from lang in langArr
            from  c in lang
            group c by c into g
            select new{g.Key, Count = g.Count()}; */

//链式.Select()
var res3 = langArr.SelectMany(c=> c).GroupBy(c=>c);

//Single,SingleOrDefault,First,Take,Skip

/* var numList = new List<int>{1,2,3,4,5,6,7,8,9,0};
var res4 = numList.Skip(3).Take(3); */


//连表
var stuList1 = new List<Student>(){
    new Student(){StuId=1,StuName="张三",Gender=false,ClassId=1},
    new Student(){StuId=2,StuName="李四",Gender=true,ClassId=2},
    new Student(){StuId=3,StuName="王五",Gender=false,ClassId=1}
};

var clsList = new List<Classes>(){
    new Classes(){ClassId=1,ClassName="1班"},
    new Classes(){ClassId=2,ClassName="2班"}
};
/* 
    select * from stuList s join clsList c on s.ClassId = c.ClassId (where) 
 */
var res5 =  from stu in stuList1
            join cls in clsList on stu.ClassId equals cls.ClassId
            select new StuClass{
                StuName = stu.StuName,
                ClassName = cls.ClassName,
                Gender = stu.Gender?"男":"女"
            };

foreach (var item in res5)
{
    Console.WriteLine($"{item.StuName},{item.ClassName}");
}

/* 
var numList1 =new List<int>{1,2,3,4};
var res6 =  numList1.AsParallel().AsOrdered()
.Select(n=> {
	Thread.Sleep(2000);
	return n*n;
}).AsSequential(); */

for (int i = 0; i < 5; i++)
{
    for (int j = 0; j < 4; j++)
    {
        for (int k = 0; k < 3; k++)
        {
            Console.WriteLine($"{i},{j},{k}");
        }
    }
}
//使用linq打印所有排列组合
var res6 =  from i in Enumerable.Range(0,5)
            from j in Enumerable.Range(0,4)
            from k in Enumerable.Range(0,3)
            select $"({i},{j},{k})";





